'use strict';
const _var = require('../lib/variables');
 const authMiddleware = function (err,req,res,next){
	if (err.name === 'UnauthorizedError') {
		return res.status(401).send(_var.UnT);
	}
	next();
 }

 module.exports = authMiddleware;