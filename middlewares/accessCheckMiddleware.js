'use strict';
const jwt = require('jsonwebtoken'),
	_var = require('../lib/variables'),
	secret = require('../config').secret,
	accessChekcer = function (request,response, next){
		const part = request.headers.authorization.split(" "),
			token = part[1];
		jwt.verify(token, secret, (err, decode) => {
			if(err){
				console.log("<==============AccessChekcerMW-ERROR==============>");
				console.log("Hint - May be you secret is being compromised.")
				console.log(err);
				return response.status(500).send(_var.SE).end()
			}
			else{
				console.log(decode);
				request.role = decode.role[0];
				next();
			}
		})
	};

module.exports = accessChekcer;