'use strict';
const mongoose = require('mongoose');
const dbObj = {
	getDbUrl : function (dbConf) {
		console.log(dbConf);
		dbConf.host = process.env.OPENSHIFT_MONGODB_DB_HOST || dbConf.host;
		dbConf.port = process.env.OPENSHIFT_MONGODB_DB_PORT || dbConf.port;
		dbConf.dbName = dbConf.dbName || 'myDoc';
		
		const dbUrl = 'mongodb://' + dbConf.host + ':' + dbConf.port + '/' + dbConf.dbName;
		return dbUrl;
	},
	getConn : function (url){
		mongoose.connect(url);
		return mongoose.connection;
	}
}
module.exports = dbObj