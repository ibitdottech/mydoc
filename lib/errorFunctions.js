"use strict";
const errFunc = {
	duplicate : function(errMsg){
		let str = errMsg.split(" "),
			message = str[1].replace('d', 'D') + " " + str[2] + " : ",
			key = str[5].split("$"),
			field = key[1].replace('_1', '');
		message += field + " " + str[str.length - 2] + " already exist.";
		let obj = {
			code : 1,
			errmsg : message
		}
		return obj;
	},
	required : function(errObj){
		const requiredFields = Object.keys(errObj);
		const errMessages = [];
		for (var i = 0; i < requiredFields.length; i++) {
			let message = '"' + errObj[requiredFields[i]].path + '" is ' + errObj[requiredFields[i]].kind + ".";
			errMessages.push(message);
		};
		return {code : 1, errmsg : errMessages};
	}
}

module.exports = errFunc;