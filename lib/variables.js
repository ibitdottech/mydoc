const obj = {
	SE : {code : -1, "errmsg" : "Internal Server Error. Please try again later."},
	BR : {code : 1, "errmsg" :"Parameters are missing. Make sure you provide the necessary parameters."},
	ND : {code : 1, "errmsg" :"No Data Found. Invalid ID OR Query String!"},
	UnT : {code : 1, "errmsg" : "Invalid Token."},
	UKT : {code : 1, "errmsg" : "Provide token is unkown to server."},
	UA : {code : 1, "errmsg" :"Invalid Credientials."},
	NA : {code : 1, "errmsg" : "You don't have permission to perform this operation."},
	OK : {code : 0, "errmsg" :"Done Successfully!"}
}

module.exports = obj;