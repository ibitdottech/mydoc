var conf = {
    	"server": {
    		"port": 3000,
            "url" : 'http://localhost:3000/assets/imgs/'
    	},
    	"db": {
    		"dbName": "myDoc",
    		"port": 27017,
    		"host": "localhost"
    	},
       	"secret" : "DOTtechRockers",
        "sms" : {
            sid : 'ACc7efa069c5e6a6814cefd3b88881fed0',
            authToken : '6ca4e55d54ecfdb948a6562c002510f5'
        }
    };

module.exports = conf;