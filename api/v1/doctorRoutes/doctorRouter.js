'use strict';

const express = require('express'),
	_var = require('../../../lib/variables'),
	Doctor = require('../../../models/doctorModel'),
	Appointments = require('../../../models/appointmentModel'),
	md5 = require('md5'),
	errFunc = require('../../../lib/errorFunctions'),
	router = express.Router();

router
	.get('/all-doctors', (request, response) => {
		if(request.role != "doctor" && request.role != 'admin')
			return response.status(401).send({code : 1 , errmsg : "You don't have rights to perform this action."}).end();
		Doctor.find({},{password : 0})
			.then((doctorData) => {
				if(doctorData.length != 0 ){
					delete
					response.status(200).send(doctorData).end();
				}
				else
					response.status(400).send(_var.ND).end();
			})
			.catch()
	})
	.get('/my-appoinments/:ID', (request,response) => {
		if(request.role != "doctor" && request.role != 'admin')
			return response.status(401).send({code : 1 , errmsg : "You don't have rights to perform this action."}).end();
		const id = request.params.ID;
		Doctor.findById(id)
			.then(doctor => {
				if(doctor == null)
					return response.status(400).send(_var.ND).end();
				return Appointments.find({'doctor._id' : id},{"__v":0, 'doctor' : 0}).sort({"time.from" : 1});
			})
			.then(appointmentData => {
				if(appointmentData.length == 0)
					return response.status(200).send({appointmentData : [], calendarData :[] }).end();
				const bundle = {
					appointmentData : [],
					calendarData : []
				};
				for (let i = 0; i < appointmentData.length; i++) {
					bundle.appointmentData.push(appointmentData[i].toObject());
					let salutaion = "Mr. ";
					if(appointmentData[i].patient.gender == "Female")
						salutaion = "Mrs. ";
					const obj = {
						id : appointmentData[i]._id,
						title : salutaion + appointmentData[i].patient.first_name,
						start : new Date(appointmentData[i].time.from).toUTCString(),
						end : new Date(appointmentData[i].time.to).toUTCString(),
						isFulfilled : appointmentData[i].isFulfilled,
						isCanceled : appointmentData[i].isCanceled
					}
					bundle.calendarData.push(obj); 
				}
				return response.status(200).send(bundle).end();
			})
			.catch(err => {
				console.log(err);
				response.status(400).send(err).end();
			})
	})
	.get('/patient-history/:ID', (request, response) => {
		const patient_id = request.params.ID;
		if(patient_id == null || ""){
			_var.BR.errmsg += " (Patient ID is missing.)";
			return response.status(400).send(_var.BR).end();
		}
		Appointments.find({"patient._id" : patient_id, "isFulfilled" : true},{'doctor' : 0, '__v' : 0})
			.then(res => {
				if(res.length == 0)
					return response.status(200).send([]).end();
				for (let i = 0; i < res.length; i++) {
					res[i] = res[i].toObject();
					res[i].time.from = new Date(res[i].time.from).toUTCString();
					res[i].time.to = new Date(res[i].time.to).toUTCString();
				};
				return response.status(200).send(res).end();
			})
			.catch(err => {
				return response.status(500).send(_var.SE).end();
			})
	})
	.get('/:UID', (request, response) => {
		if(request.role != "doctor" && request.role != 'admin')
			return response.status(401).send({code : 1 , errmsg : "You don't have rights to perform this action."}).end();
		Doctor.getDocById(request)
			.then(docObj => {
				if(docObj == null)
					return response.status(400).send(_var.ND).end()
				return response.status(200).send(docObj).end();
			})
			.catch(err => {
				console.log("||============Error In getting DoctorByID============||");
				console.log(err);
				return response.status(500).send(_var.SE).end();
			})
	})
	.post('/add-prescription', (request, response) => {
		const appointment_id = request.body.appointment_id,
			prescription_detail = request.body.prescription.detail,
			diagnosis = request.body.prescription.diagnosis,
			diagnostics = request.body.prescription.diagnostics;
		if((appointment_id == null || "") || (prescription_detail == null || ""))
			return response.status(400).send(_var.BR).end();
		Appointments.findById(appointment_id)
			.then(res => {
				if(res == null)
					return response.status(400).send(_var.ND).end();
				res.prescription.detail = prescription_detail;
				res.prescription.diagnosis = diagnosis || "No Diagnosis";
				res.prescription.diagnostic = diagnostics || "No Diagnostics";
				res.isFulfilled = true;
				return res.save();
			})
			.then(res => {
				response.status(200).send(_var.OK).end();
			})
			.catch(err => {
				let res = {};
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				response.status(status).send(res).end();
			})
	})
	.post('/update-profile', (request, response) => {
		if(request.role != "doctor" && request.role != 'admin')
			return response.status(401).send({code : 1 , errmsg : "You don't have rights to perform this action."}).end();
		const id = request.body._id;
		if(id == null || "")
			return response.status(400).send(_var.BR).end();
		Doctor.updateDoctor(request)
			.then(updateDoctor => {
				response.status(200).send(updateDoctor).end();
			})
			.catch((err,status) => {
				response.status(500).send(err).end();
			})
	})
	.post('/cancel-appointment', (request, response) => {
		console.log(request.body);
		const appointment_id = request.body.appointment_id;
		if(appointment_id == null || ""){
			_var.BR += " (Appointment ID is required.)";
			return response.status(400).send(_var.BR).end();
		}
		Appointments.findById(appointment_id)
			.then(res => {
				if(res == null)
					return response.status(400).send(_var.ND).end();
				res.isFulfilled = false;
				res.isCanceled = true;
				return res.save()
			})
			.then(res => {
				return response.status(200).send(_var.OK).end();
			})
			.catch(err => {
				let res = {};
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				return response.status(status).send(res).end();
			})
	})
	// .post('forgot-password', (request, response) => {})
	// .post('/change-password', (request, response) => {})
module.exports = router;
