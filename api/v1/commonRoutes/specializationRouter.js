'use strict';
const express = require('express'),
	_var = require('../../../lib/variables'),
	router = express.Router(),
	Specialization = require('../../../models/specializationModel');

router
	.get('/all-specializations', (request, response) => {
		Specialization.find({}, {__v : 0})
			.then(res => {
				if(res.length == 0)
					return response.status(400).send({code : 1, message : 'No specialization found.'}).end();
				return response.status(200).send(res)
			})
			.catch(err => {
				console.log(err);
				return response.status(500).send(_var.SE).end();
			})
	})
module.exports = router;