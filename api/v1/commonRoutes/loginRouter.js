'use strict';
const express = require('express'),
	md5 = require('md5'),
	_var = require('../../../lib/variables'),
	secret = require('../../../config').secret,
	Doctor = require('../../../models/doctorModel'),
	router = express.Router(),
	jwt = require('jsonwebtoken'),
	User = require('../../../models/userModel');

router
	.post('/login', (request, response) => {
		const username = request.body.username,
			password = request.body.password;
		if((username == null || "") || (password == null || ""))
			return response.status(400).send(_var.BR).end();
		User.findOne({username : username, password : md5(password)},{password : 0, __v: 0})
			.then(userData => {
				let bundle = {};
				if(userData == null)
					return response.status(400).send(_var.ND).end();
				userData = userData.toObject();
				if(userData.role[0] == 'doctor'){
					Doctor.findById(userData._id,{"password" : 0, "__v" : 0})
						.then(res => {
							userData = res.toObject();
							bundle.token = jwt.sign(userData,secret);
							bundle.user = userData;
							return response.status(200).send(bundle).end();
						});
				}else{
					bundle.token = jwt.sign(userData,secret);
					bundle.user = userData;
					return response.status(200).send(bundle).end();
				}
			})
			.catch(err => {
				console.log("<==============LoginServerError==============>");
				console.log(err);
				response.status(500).send(_var.SE).end();
			})
	})

module.exports = router;