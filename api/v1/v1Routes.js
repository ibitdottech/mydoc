'use strict';
const express = require('express'),
	router = express.Router(),
	patientRouter = require('./patientRoutes/patientRouter'),
	accessChecker = require('../../middlewares/accessCheckMiddleware'),
	path = require('path'),
	config = require('../../config').server,
	loginRouter = require('./commonRoutes/loginRouter'),
	specializationRouter = require('./commonRoutes/specializationRouter'),
	adminRouter = require('./adminRoutes/adminRouter'),
	multer = require('multer'),
	doctorRouter = require('./doctorRoutes/doctorRouter');

const storage = multer.diskStorage({
	destination : function(req, file, cb){
		var dest = path.resolve(path.join(__dirname,'/../../public/assets/imgs'));
		console.log(dest);
		cb(null, dest);
	},
	filename : function(req, file,cb){
		var datestamp = Date.now();
		var fileName = file.fieldname + '-' + datestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1];
		req.profile_pic = config.url + fileName;
		cb(null, fileName);
	},
	onFileUploadStart: function (file) {
		console.log(file.fieldname + ' is starting ...');
	},
	onFileUploadComplete: function (file) {
		console.log(file.fieldname + ' uploaded to  ' + file.path)
	}
});
var upload = multer({
	storage : storage
}).single('file');
router.post('/user/uploads',upload, function(request, response){
	console.log(request.profile_pic);
	return response.status(200).send({url : request.profile_pic || "ok"}).end();
});
router.use('/doctor',accessChecker, doctorRouter);
router.use('/admin',accessChecker, adminRouter),
router.use('/patient',patientRouter);
router.use('/specialization', specializationRouter);
router.use('/user',loginRouter);
module.exports = router;