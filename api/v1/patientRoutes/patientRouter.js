'use strict';

const express = require('express'),
	_var = require('../../../lib/variables'),
	Doctor = require('../../../models/doctorModel'),
	Appointments = require('../../../models/appointmentModel'),
	User = require('../../../models/userModel'),
	md5 = require('md5'),
	errFunc = require('../../../lib/errorFunctions'),
	router = express.Router();

router
	.post('/make-appointment', (request, response) => {
		const doctorId = request.body.doctorId;
		const patientId = request.user._id;
		const time = request.body.time;
		if((doctorId == null || "") || (patientId == null || "") || (time.from == null || "") || (time.to == null || ""))
			return response.status(400).send(_var.BR).end();
		if(new Date (time.from) == "Invalid Date")
			return response.status(400).send({code : 1, errmsg : "\'From\' time must be a valid UTC date string"});
		if(new Date (time.to) == "Invalid Date")
			return response.status(400).send({code : 1, errmsg : "\'To\' time must be a valid UTC date string"});
		let globalPatient = {};
		User.findById(patientId)
			.then(patient => {
				if(patient == null){
					_var.ND.errmsg += " (patientId)";
					return response.status(400).send(_var.ND).end();
				}
				globalPatient = patient.toObject();
				delete globalPatient.__v;
				delete globalPatient.password;
				return Doctor.findById(doctorId);
			})
			.then(doctor => {
				if(doctor == null){
					_var.ND.errmsg += " (doctorId)";
					return response.status(400).send(_var.ND).end();
				}
				doctor = doctor.toObject();
				delete doctor.__v;
				delete doctor.password;
				const newAppointment = new Appointments({
					doctor : doctor,
					patient : globalPatient,
					time : {
						from : new Date(request.body.time.from),
						to : new Date(request.body.time.to)
					},
					prescription : {
						detail : "No Prescription",
						diagnosis : "No diagnosis",
						diagnostic : "No diagnostics",
					},
					isBooked : true,
					isCanceled : false,
					isFulfilled : false
				});
				return newAppointment.save();
			})
			.then(newAppointment => {
				return response.status(200).send(_var.OK).end();
			})
			.catch(err => {
				let res = {};
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				response.status(status).send(res).end();
			});
	})
	.get('/my-history', (request,response) => {
		const uid = request.user._id;
		console.log(uid);
		Appointments.find({"patient._id" : uid, $or : [{isFulfilled : true}, {isCanceled : true}]}, {"patient" : 0, "__v" : 0}).sort({"time.from" : -1})
		.then(res => {
			console.log(res.length);
			return response.status(200).send(res).end();
		})
		.catch(err => {
			console.log(err);
			return response.status(500).send(_var.SE).end();
		})
	})
	.get('/doctors/:specilization', (request, response) => {
		const specialization = request.params.specilization;
		if(specialization == null || "")
			return response.status(400).send(_var.BR).end();
		Doctor.find({specialization : { $all: [specialization] }},{role : 0, password : 0, due_date : 0,})
			.then(res => {
				if(res.length === 0)
					return response.status(400).send(_var.ND).end();
				console.log(res);
				for(var i = 0; i< res.length; i++){
					res[i] = res[i].toObject();
					res[i].days_string = res[i].availability.days.toString();
					res[i].spec_string = res[i].specialization.toString();
					res[i].contacts_string = res[i].contacts.toString();
					var obj = res[i].availability.from.split(':');
					var obj2 = res[i].availability.to.split(':');
					var from_hours = parseInt(obj[0]);
					var to_hours = parseInt(obj2[0]);
					if(obj[1].indexOf('P') != -1 && from_hours < 12)
						res[i].availability.from_in_hours = (from_hours + 12) % 24;
					else
						res[i].availability.from_in_hours = from_hours % 24;
					if(obj2[1].indexOf('P') != -1 && to_hours < 12)
						res[i].availability.to_in_hours = (to_hours + 12) % 24;
					else
						res[i].availability.to_in_hours = to_hours % 24;
				}
				return response.status(200).send(res).end();
			})
			.catch(err => {
				let res = {};
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				response.status(status).send(res).end();
			});
	})
	.get('/my-appointments', (request, response) => {
		const uid = request.user._id;
		Appointments.find({"patient._id" : uid, $and : [{'isFulfilled' : false},{'isCanceled' : false}]}).sort({'time.from' : -1})
			.then(res => {
				return response.status(200).send(res).end();
			})
			.catch(err => {
				console.log(err);
				return response.status(500).send(_var.SE).end();
			})
	})
	.get('/doctor-appointments/:id', (request,response) => {
		const doctor_id = request.params.id,
			patient_id = request.user._id;
		let from = request.query.from;
		if((doctor_id == null || "") || (from == null || ""))
			return response.status(400).send(_var.BR).end();
		if(new Date(from) == "Invalid Date")
			return response.status(400).send(_var.BR).end();
		from = new Date(from);
		Appointments.find({'doctor._id' : doctor_id, 'time.from' : {$gte : from}})
			.then(res => {
				if(res.length == 0)
					return response.status(200).send([]).end();
				const appointments = res.map(function (app){
					const obj = {};
					if (app.patient._id == patient_id)
						obj.yours = true;
					else
						obj.yours = false;
					obj.id = app._id;
					let date = new Date(app.time.from);
					obj.from = date.toUTCString();
					date = new Date(app.time.to);
					obj.to = date.toUTCString();
					return obj;
				})
				return response.status(200).send(appointments).end();
			})
			.catch(err => {
				console.log(err);
				return response.status(500).send(_var.SE).end();
			})
	})
	.post('/register', (request, response) => {
		if(request.body.password.length < 8 ){
			return response.status(400).send({code : 1, message : "Password must be 8 Character long."});
		}
		const newPatient = new User({
			"first_name" : request.body.first_name,
			"last_name" : request.body.last_name,
			"profile_pic" : request.body.profile_pic,
			"username" : request.body.username,
			"gender" : request.body.gender,
			"marital_status" : request.body.marital_status,
			"dob" : new Date(request.body.dob),
			"joined_date" : new Date(request.body.joined_date),
			"contacts" : request.body.contacts,
			"role" : ['patient'],
			"email" : request.body.email,
			"password" : md5(request.body.password),
			"address" : {
				"address_detail" : request.body.address.address_detail,
				"city" : request.body.address.city,
				"state" : request.body.address.state,
				"country" : request.body.address.country
			}
		})
		newPatient.save()
			.then(res => {
				console.log(res);
				response.status(200).send(_var.OK).end();
				// Send Token Here;
			})
			.catch(err => {
				let res = {};
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				response.status(status).send(res).end();
				// response.status(500).send(err).end();
			})
	})
	.post('/cancel-appointment', (request,reponse) => {
		const appointment_id = request.body.appointment_id;
		if(appointment_id == null || "")
			return response.status(400).send(_var.BR).end();
		Appointments.findById(appointment_id)
			.then(res => {
				if(res == null)
					return response.status(400).send(_var.ND).end();
				res.isCanceled = true;
				res.isFulfilled = false;
				return res.save();
			})
			.then(res => {
				return response.status(200).send(_var.OK).end();
			})
			.catch(err => {
				let res = _var.SE;
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				response.status(status).send(res).end();
			})
	})
	.get('/username/:username',(request, response) => {
		const uName = request.params.username;
		User.find({username : uName}, {username : 1})
			.then(data => {
				if (data.length > 0)
					return response.status(400).send(_var.BR).end();
				return response.status(200).send(_var.OK).end();
			})
			.catch(err => {
				console.log(err);
				return response.status(500).send(_var.SE).end();
			})
	})


module.exports = router;