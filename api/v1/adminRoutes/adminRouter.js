'use strict';
const express = require('express'),
	User = require('../../../models/userModel'),
	Appointment = require('../../../models/appointmentModel'),
	Doctor = require('../../../models/doctorModel'),
	jwt = require('jsonwebtoken'),
	secret = require('../../../config').secret,
	_var = require('../../../lib/variables'),
	errFunc = require('../../../lib/errorFunctions'),
	Specialization = require('../../../models/specializationModel'),
	md5 = require('md5'),
	router = express.Router();

router
	.post('/add-doctor', (request, response) => {
		const now = new Date();
		if(request.role != 'admin')
			return response.status(401).send({code : 1 , errmsg : "You don't have rights to perform this action."}).end();
		if(request.body.password.length < 8 )
			return response.status(400).send({code : 1, errmsg : "Password must be 8 Character long."});
		const user = {
			"first_name" : request.body.first_name,
			"last_name" : request.body.last_name,
			"profile_pic" : request.body.profile_pic,
			"username" : request.body.username,
			"gender" : request.body.gender,
			"marital_status" : request.body.marital_status,
			"dob" : new Date(request.body.dob),
			"joined_date" : new Date(request.body.joined_date),
			"contacts" : request.body.contacts,
			"role" : ['doctor'],
			"email" : request.body.email,
			"password" : md5(request.body.password),
			"address" : {
				"address_detail" : request.body.address.address_detail,
				"city" : request.body.address.city,
				"state" : request.body.address.state,
				"country" : request.body.address.country
			}
		};
		const docAttribute = {
			"specialization" : request.body.specialization,
			"type" : request.body.type,
			"about" : request.body.about,
			"availability" : {
				"from" : request.body.availability.from,
				"to" : request.body.availability.to,
				"slot" : parseInt(request.body.availability.slot),
				"days" : request.body.availability.days
			},
			"due_date" : request.body.due_date || new Date(now.toUTCString())
		}
		let userAdded = false;
		let globalUser = {};
		const newUser = new User(user);
		newUser.save()
			.then(userData => {
				globalUser = userData;
				userData = userData.toObject();
				delete userData.__v;
				userAdded = true;
				const doctorData = Object.assign(userData,docAttribute);
				const newDoctor = new Doctor(doctorData);
				return newDoctor.save();
			})
			.then(docData => {
				response.status(200).send(_var.OK).end()
			})
			.catch(err => {
				if(userAdded){
					User.findOneAndRemove({_id : globalUser._id})
						.then(res => {
							console.log("==============Deleted User==============");
							console.log(res);
						})
						.catch(err => {
							console.log("==============Error In Deleting User==============");
							console.log(err);
						})
				}
				let res = {};
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				response.status(status).send(res).end();
			})
	})
	.post('/add-specialization', (request, response) => {
		if(request.role != 'admin')
			return response.status(401).send({code : 1 , errmsg : "You don't have rights to perform this action."}).end();
		const specialization = request.body.name;
		if(specialization == null || '')
			return response.status(400).send(_var.BR).end();
		const newSpecialization = new Specialization({
			name : specialization
		});
		newSpecialization.save()
			.then(res => {
				response.status(200).send(_var.OK).end();
			})
			.catch(err => {
				let res = _var.SE;
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				return response.status(status).send(res).end();
			})
	})
	.post('/remove-specialization', (request, response) => {
		if(request.role != 'admin')
			return response.status(401).send({code : 1 , errmsg : "You don't have rights to perform this action."}).end();
		const spec_id = request.body._id;
		if(spec_id == null || "")
			return response.status(400).send(_var.BR).end();
		Specialization.findOneAndRemove({_id : spec_id})
			.then(res => {
				if(res == null)
					return response.status(400).send(_var.ND).end();
				return response.status(200).send(_var.OK).end();
			})
			.catch(err => {
				console.log(err);
				return response.status(500).send(_var.SE).end();
			})
	})
	.post('/update-specialization', (request, response) => {
		console.log("Inside update-specialization");
		if(request.role != 'admin')
			return response.status(401).send({code : 1 , errmsg : "You don't have rights to perform this action."}).end();
		const spec_id = request.body._id;
		const spec_name = request.body.name;
		if((spec_id == null || "") || (spec_name == null || ""))
			return response.status(400).send(_var.BR).end();
		Specialization.findOneAndUpdate({_id : spec_id},{name : spec_name})
			.then(res => {
				if(res == null)
					return response.status(400).send(_var.ND).end();
				return response.status(200).send(_var.OK).end();
			})
			.catch(err => {
				let res = {};
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				response.status(status).send(res).end();
			})

	})
	// .post('/deleteDoctor', (request, response) => {
	// 	if(request.role != 'admin')
	// 		return response.status(401).send(_var.NA).end();
	// 	const id = request.body.uid;
	// 	if(uid == null || '') return response.status(400).send(_var.BR).end();
	// 	// User.findOneAndRemove({_id : id})
	// 	// 	.then()
	// })
	.post('/add-admin', (request,response) => {
		const user = {
			"first_name" : request.body.first_name,
			"last_name" : request.body.last_name,
			"username" : request.body.username,
			"contacts" : request.body.contacts,
			"role" : ['admin'],
			"email" : request.body.email,
			"password" : md5(request.body.password),
			"address" : {
				"address_detail" : request.body.address.address_detail,
				"city" : request.body.address.city,
				"country" : request.body.address.country,
				"postal_code" : request.body.address.postal_code
			}
		};
		const newUser = new User(user);
		newUser.save()
			.then(adminData => {
				adminData = adminData.toObject();
				delete adminData.password;
				adminData.token = jwt.sign(adminData,secret);
				response.status(200).send(adminData).end();
			})
			.catch(err => {
				let res = {};
				let status = 500;
				if(err.code == 11000){
					res = errFunc.duplicate(err.errmsg);
					status = 400;
				}
				if(err.name == "ValidationError"){
					res = errFunc.required(err.errors);
					status = 400;
				}
				response.status(status).send(res).end();
			})
	})

module.exports = router;