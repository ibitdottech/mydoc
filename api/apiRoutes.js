'use strict';
const express = require('express'),
	router = express.Router(),
	jwt = require('jsonwebtoken'),
	expressJWT = require('express-jwt'),
    authMW = require('../middlewares/authMiddleware'),
    secret = require('../config').secret,
	v1Routes = require('./v1/v1Routes');

router.use(expressJWT({secret : secret}).unless({path : ['/api/v1/user/login','/api/v1/patient/register','/api/v1/user/uploads']}));
router.use(authMW);
router.use('/v1', v1Routes);
module.exports = router;