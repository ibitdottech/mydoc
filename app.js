'use strict';
//====================================================================================
//							NameSpace - Section
//====================================================================================
const express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	dbObj = require('./lib/dbConnector'),
	apiRoutes = require('./api/apiRoutes'),
	multer = require('multer'),
	config = require('./config'),
	mongoose = require('mongoose');

//====================================================================================
//							Database - Section
//====================================================================================
const dbUrl = dbObj.getDbUrl(config.db),
	connection = dbObj.getConn(dbUrl);

connection
	.on('open', function(){
		console.log("Succefully Connected to database. Conf: ", config.db);
	})
	.on('error',function(err){
		console.log("An Error Has Occuured. " + err);
	});

//====================================================================================
//							EnvironmentSetup - Section
//====================================================================================
	
app.use(express.static('./public'));
app.use(function (request, response, next) {
	response.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
	response.header("Access-Control-Allow-Origin", "*");
	response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});
// const urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//====================================================================================
//							Routes - Section
//====================================================================================

app.use('/api', apiRoutes);

//====================================================================================
//							Server - Section
//====================================================================================
	
const port = process.env.OPENSHIFT_NODEJS_PORT || config.server.port;
app.listen(port, function (err,result){
  if(!err){
    console.log("MyDocto App listening on port: ", port);
  }
});