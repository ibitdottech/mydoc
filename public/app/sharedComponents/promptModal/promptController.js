angular.module('myDoctor')
	.controller('promptController', ['$scope','$uibModalInstance','payload', function ($scope, $uibModalInstance, payload){
		console.log(payload)
		$scope.obj = payload;
		$scope.yes = function (){
			$uibModalInstance.close(true);
		}
		$scope.cancel = function(){
			$uibModalInstance.close(false);
		}
	}])