angular.module("myDoctor")
	.service('profileService', ['$http', function  ($http){
		var req = {};
		this.login = function (data){
			req = {
				'method' : 'POST',
				'url' : '/users/login',
				'header' : {
					"Content-Type" : "application/json"
				},
				"skipAuthorization": true,
				'data' : data
			}
			return $http(req);
		}
	}])