angular.module('myDoctor')
	.controller('loginController',['$scope','$rootScope','authService','$state','$location',function ($scope,$rootScope,authService,$state,$location) {
		$scope.userData = {};
		$scope.login = function(form){
			if(form.$invalid){
				form.username.$setDirty();
				form.password.$setDirty();
			}else{
				authService.loginUser($scope.userData)
				.then(function(res){
					$rootScope.User = res.user;
					if(res.user.role[0] == 'patient')
						$state.go('patient-appointment');
					if(res.user.role[0] == 'doctor')
						$state.go('doctor-appointment');
					if(res.user.role[0] == 'admin')
						$state.go('admin');
				})
				.catch(function(err) {
					$scope.serverResponse = err.errmsg;
					$scope.loginError = true;
					form.username.$setDirty();
					form.username.$invalid = true;
					form.password.$setDirty();
					form.password.$invalid = true;
					$scope.userData = null;
				})
			}
		}
		$scope.register = function(form){
			$state.go('register');
		}
	}])