angular.module("myDoctor")
	.service('loginService', ['$http', function  ($http,Values){
		var req = {};
		this.login = function (data){
			req = {
				'method' : 'POST',
				'url' : Values.baseUrl + '/users/login',
				'header' : {
					"Content-Type" : "application/json"
				},
				"skipAuthorization": true,
				'data' : data
			}
			return $http(req);
		}
	}])