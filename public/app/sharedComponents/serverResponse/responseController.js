angular.module('myDoctor')
	.controller('responseController', ['$scope','$uibModalInstance','payload', function ($scope, $uibModalInstance, payload){
		$scope.obj = payload.data;
		console.log($scope.obj)
		if($scope.obj.code == 1){
			$scope.obj.title = "Bad Request"
		}else if($scope.obj.code == -1){
			$scope.obj.title = "Server Error.";
		}else{
			$scope.obj.title = "Success";
		}
		if(Array.isArray($scope.obj.errmsg)){
			$scope.isArray = true;
		}else{
			$scope.isArray = false;
		}
		$scope.ok = function (){
			$uibModalInstance.close(true);
		}
	}])