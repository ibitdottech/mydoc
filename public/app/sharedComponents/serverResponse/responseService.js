angular.module('myDoctor')
	.service('responseService',['$uibModal', function($uibModal){
	this.openModal = function(err){
		const modalInstance = $uibModal.open({
										animation : true,
										templateUrl : './app/sharedComponents/serverResponse/responseView.html',
										controller : 'responseController',
										size : 'sm',
										resolve : {
											payload : function (){
												return err;
											}
										}
									})
									return modalInstance.result;
	}
	}])