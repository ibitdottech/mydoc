angular.module('myDoctor')
	.service('getSpecializationService', ['$http', 'Values', function  ($http,Values){
		var req = {};
		this.getAll = function (){
			req = {
				'method' : 'GET',
				'url' : Values.baseUrl + '/specialization/all-specializations',
			}
			return $http(req);
		}
	}])