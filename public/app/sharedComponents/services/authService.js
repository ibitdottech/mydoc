angular.module('myDoctor')
	.service('authService', ['$http','$q','aiStorage','Values',function ($http, $q, aiStorage, Values){
		var loggedInUser = {},
			baseUrl = Values.baseUrl;
		this.getUser = function(){
			this.loggedInUser = aiStorage.get('userData');
			if(this.loggedInUser)
				this.loggedInUser = this.loggedInUser.userDetail;
			return this.loggedInUser;
		}
		this.getRole = function(){
			var user = aiStorage.get('userData');
			if(!user)
				return false;
			return user.userDetail.role[0];
		}
		this.loginUser = function(user){
			var defered = $q.defer();
			var req = {
				'method' : 'POST',
				'data' : user,
				"skipAuthorization": true,
				'url' : baseUrl + '/user/login'
			}
			$http(req)
				.then(function (user){
					this.loginUser = user.data;
					aiStorage.set('userData' , {'token' : user.data.token, 'userDetail' : user.data.user});
					defered.resolve(user.data);
				})
				.catch(function (err){
					defered.reject(err.data);
				})
			return defered.promise;
		}
		this.logoutUser = function(){
			aiStorage.remove('userData');
		}
	}])