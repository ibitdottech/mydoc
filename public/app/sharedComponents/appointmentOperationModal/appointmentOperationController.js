angular.module('myDoctor')
	.controller('appointmentOperationController', ['$scope','$uibModalInstance','payload', function ($scope, $uibModalInstance, payload){
		console.log(payload)
		$scope.obj = payload;
		$scope.yes = function (){
			$uibModalInstance.close(true);
		}
		$scope.cancel = function(){
			$uibModalInstance.dismiss(false);
		}
		$scope.ok = function(){
			$uibModalInstance.close();
		}
	}])