// angular.module('feedFormulation',['ngResource','ngCookies','ui.router','ui.bootstrap', 'angular-storage', 'angular-jwt'])
angular.module('myDoctor',['ngResource', 'angularjs-dropdown-multiselect', 'ui.router', 'permission', 'ui.bootstrap', 'angularValidator', 'ngFileUpload', 'angular-storage', 'angular-jwt'])
	.config(function ($stateProvider,jwtInterceptorProvider, $urlRouterProvider, $httpProvider) {
		jwtInterceptorProvider.tokenGetter = function (aiStorage) {
			const user = aiStorage.get('userData');
			if(user){
				const token = user.token;
				return token;
			}
		}
		$httpProvider.interceptors.push('jwtInterceptor');
		const baseUrl = "./app/components";
		$stateProvider
				.state('login', {
					url : '/login',
					controller : "loginController",
					templateUrl :   "./app/sharedComponents/login/loginView.html",
					data : {'isPublic' : true}
				})
				.state('register', {
					templateUrl : baseUrl + '/patient/register/registerView.html',
					url : '/register-patient',
					controller : 'registerController'
				})
				.state('dashboard', {
					templateUrl : './app/dashboard.html',
					abstract : true,
				})	
				.state('doctor', {
					templateUrl : baseUrl + '/doctor/mainDoctor.html',
					url : '/doctor',
					parent : 'dashboard',
					data: {
						permissions: {
							only: ['doctor'],
							redirectTo : 'login'
						}
					}
				})
				.state('add-prescription', {
					templateUrl : baseUrl + '/doctor/addPrescription/addPrescriptionView.html',
					url : '/add-prescription',
					controller : 'addPrescriptionController',
					parent : 'doctor',
					data: {
						permissions: {
							only: ['doctor'],
							redirectTo : 'login'
						}
					}
				})
				.state('patient', {
					templateUrl : baseUrl + '/patient/mainPatient.html',
					url : '/patient',
					parent : 'dashboard',
					data: {
						permissions: {
							only: ['patient'],
							redirectTo : 'login'
						}
					}
				})
				.state('search-doctor', {
					url : '/search-doctor',
					templateUrl : baseUrl + '/patient/searchDoctor/searchDoctorView.html',
					controller : "searchDoctorController",
					parent : 'patient',
					data: {
						permissions: {
							only: ['patient'],
							redirectTo : 'login'
						}
					}
				})
				.state('view-profile',{
					url: '/my/view-profile',
					templateUrl: './app/sharedComponents/profile/profileView.html',
					controller: "profileController",
					parent:'dashboard'
				})
				.state('patient-appointment', {
					url : '/my-appointment',
					templateUrl : baseUrl + '/patient/appointments/myAppointment/patientAppointmentView.html',
					parent : 'patient',
					controller : 'patientAppointmentController',
					data: {
						permissions: {
							only: ['patient'],
							redirectTo : 'login'
						}
					}
				})
				.state('patient-appointment-history',{
					url : '/my-history',
					templateUrl : baseUrl + '/patient/appointments/appointmentHistory/patientHistoryView.html',
					parent : 'patient',
					controller : 'patientHistoryController',
					data: {
						permissions: {
							only: ['patient'],
							redirectTo : 'login'
						}
					}
				})
				.state('doctor-appointment', {
					url : '/my-appointment',
					templateUrl : baseUrl + '/doctor/appointments/myAppointment/doctorAppointmentView.html',
					parent : 'doctor',
					controller : 'doctorAppointmentController',
					data: {
						permissions: {
							only: ['doctor'],
							redirectTo : 'login'
						}
					}
				})
				.state('admin', {
					url : '/admin',
					parent : 'dashboard',
					templateUrl : baseUrl + '/admin/mainAdmin.html',
					data: {
						permissions: {
							only: ['admin'],
							redirectTo : 'login'
						}
					}
				})
				.state('specialization', {
					url : '/specialization',
					controller : 'specializationController',
					parent : 'admin',
					templateUrl : baseUrl + '/admin/manageSpecialization/specialzationView.html',
					data: {
						permissions: {
							only: ['admin'],
							redirectTo : 'login'
						}
					}
				})
				.state('admin-search-doctor', {
					url : '/search-doctor',
					templateUrl : baseUrl + '/admin/search/searchDoctor/searchDoctorView.html',
					controller : 'searchDoctorController',
					parent : 'admin',
					data: {
						permissions: {
							only: ['admin'],
							redirectTo : 'login'
						}
					}
				})
				.state('patient-history', {
					url : '/patient-history',
					templateUrl : baseUrl + '/doctor/patientHistory/patientHistoryView.html',
					parent : 'doctor',
					controller : 'patientHistoryController',
					data: {
						permissions: {
							only: ['doctor'],
							redirectTo : 'login'
						}
					}
				})
				.state('add-doctor', {
					url : '/add-doctor',
					templateUrl : baseUrl + '/admin/addDoctor/addDoctorView.html',
					parent : 'admin',
					controller : 'addDoctorController',
					data: {
						permissions: {
							only: ['admin'],
							redirectTo : 'login'
						}
					}
				})

		$urlRouterProvider.otherwise('/login');
	})
	.run(function(PermissionStore, authService, $rootScope, $state){
		$rootScope.User = authService.getUser();
		$rootScope.logout = function (){
			authService.logoutUser();
			$rootScope.User = {};
			$state.go('login');
		}
		PermissionStore.definePermission('anonymous', function(stateParams){
			if( authService.getUser() == null ){
				return true;
			}
		})
		PermissionStore.definePermission('patient', function(stateParams){
			if( authService.getRole() == 'patient')
				return true;
			else
				return false;
		})
		PermissionStore.definePermission('admin', function(stateParams){
			if( authService.getRole() == 'admin')
				return true;
			else
				return false;
		})
		PermissionStore.definePermission('doctor', function(stateParams){
			if( authService.getRole() == 'doctor')
				return true;
			else
				return false;
		})
	})
	// .run(['$rootScope', '$state', function ($rootScope,$state){
	// 	$rootScope
	// 		.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams){
	// 		})
	// 	$rootScope.logout = function (){
	// 		authService.logoutUser();
	// 		$state.go('login');
	// 	}
	// }])