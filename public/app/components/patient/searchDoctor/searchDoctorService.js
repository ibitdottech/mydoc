angular.module('myDoctor')
	.service('searchDoctorService', ['$http', 'Values', function  ($http,Values){
		var req = {};
		this.search = function (query){
			req = {
				'method' : 'GET',
				'url' : Values.baseUrl + '/patient/doctors/' + query,
			}
			return $http(req);
		}
		this.registerAppointment = function(data){
			req = {
				"method" : 'POST',
				"url" : Values.baseUrl + "/patient/make-appointment",
				"data" : data 
			}
			return $http(req);
		}
		this.getAppointments = function(data){
			req = {
				"method" : 'GET',
				"url" : Values.baseUrl + "/patient/doctor-appointments/" + data.doc_id + "?from=" + data.date,
			}
			return $http(req);
		}
	}])