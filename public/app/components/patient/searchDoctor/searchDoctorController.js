angular.module('myDoctor')
	.controller('searchDoctorController',['$scope', 'searchDoctorService', 'responseService', 'getSpecializationService', '$state', '$uibModal', '$location', function ($scope, searchDoctorService, responseService, getSpecializationService, $state, $uibModal, $location) {
		var jq = $.noConflict();
		var now = new Date();
		$scope.spec_list = [];
		getSpecializationService.getAll()
			.then(function(res){
				$scope.spec_list = res.data;
			})
			.catch(function(err){
				responseService.openModal(err)
					.then(function(result){
						console.log(result);
					})
					.catch(function(result){
						console.log(result);
					})
			})
		$scope.getDocs = function(){
			$scope.onCalendar = false;
			jq('#calendar').fullCalendar('destroy');
			searchDoctorService.search($scope.queryName)
				.then(function (res){
					console.log(res);
					$scope.doc_list = res.data;
				})
				.catch(function(err){
					$scope.doc_list = false;
					responseService.openModal(err)
						.then(function(result){
							console.log(result);
						}, function(result){
							console.log(result);
						})
				})
		}
		$scope.dayClick = function(date, jsEvent, view){
			console.log(date);
			var labelForModal = date.format('DD MMM, YYYY, hh:mm a');
			var now = moment();
			var payload = {};
			if(date < now){
				payload.description = "Can not make Appointment in the passed Date. Dr. " +$scope.doc_list[0].first_name +" is available between " + $scope.doc_list[0].availability.from + " To : " + $scope.doc_list[0].availability.to,
				payload.title = "Appointment Error";
				payload.error = true;
				var modal = open('lg', payload);
				modal.result.then(function(res){
					console.log(res);
				})
				return;
			}

			if(date.get('hours') < parseInt($scope.doc_list[0].availability.from_in_hours) || date.get('hours') > parseInt($scope.doc_list[0].availability.to_in_hours)){
				console.log("Get Hours", date.get('hours'));
				console.log(date.get('hours') < parseInt($scope.doc_list[0].availability.from_in_hours));
				console.log(date.get('hours') > parseInt($scope.doc_list[0].availability.to_in_hours));
				payload.description = "You Can not make the appointment at selected slot. Dr. " + $scope.doc_list[0].first_name +" is available between " + $scope.doc_list[0].availability.from + " To " + $scope.doc_list[0].availability.to,
				payload.title = "Appointment Error",
				payload.error = true;
				var modal = open('lg', payload);
				modal.result.then(function(res){
					console.log(res);
				})
				return;
			}
			var from = date.utc().toString();
			console.log("fromOBJ" , date.utc());
			console.log("from", from);
			var to = date.add($scope.doc_list[0].availability.slot, 'minute');
			console.log("toOBJ", to.utc());
			console.log("to", to.utc().toString());
			var obj = {
				doctorId : $scope.doc_list[0]._id,
				time : {
					from : from,
					to : to.utc().toString()
				}
			}
			console.log("obj", obj);
			payload.description = "Confirm appointment with Dr. " + $scope.doc_list[0].first_name + " at " + labelForModal;
			payload.title = "Appointment Confirmation";
			payload.error = false;
			var modal = open('lg', payload);
			modal.result.then(function(res){
				return searchDoctorService.registerAppointment(obj);
			})
			.then(function(res){
				responseService.openModal(res);
				$scope.getAppointmentsList($scope.doc_list[0]._id);
			})
			.catch(function(err){
				if(typeof(err) != 'boolean'){
					responseService.openModal(err);
				}else{
					console.log(err);
				}
			})
		}
		$scope.eventClick = function(calEvent, jsEvent, view){
				console.log(calEvent);
			// if(calEvent.yours)
			// else
			// 	console.log("not Mine");
		}
		$scope.getCalendar = function(appointments){
			jq('#calendar').fullCalendar({
				eventClick: $scope.eventClick,
				dayClick: $scope.dayClick,
				timezone : 'local',
				events : appointments,
				slotDuration : '00:' + $scope.doc_list[0].availability.slot,
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				buttonText: {
					today: 'today',
					month: 'month',
					week: 'week',
					day: 'day'
				},
				editable: false,
				eventOverlap : false,
				droppable: false,
			})
			jq('#calendar').fullCalendar( 'render' );
		}
		$scope.getAppointmentsList = function(doc_id){
			var data = {
				doc_id : doc_id,
				date : now.toUTCString()
			}
			searchDoctorService.getAppointments(data)
				.then(function(res){
					var appointments = [];
					if(res.data.length != 0){
						appointments = res.data.map(function (app){
							if(app.yours){
								app.title = "Your Appintment";
								app.backgroundColor = "#0073b7";
								app.borderColor = "#0073b7";
							}
							else{
								app.backgroundColor = "#f56954";
								app.borderColor = "#f56954";
								app.title = "Reserved Appointment";
							}
							app.start = new Date(app.from);
							app.end = new Date(app.to);
							app.allDay = false;
							app.editable = false;
							delete app.from;
							delete app.to;
							return app;
						})
					}
					$scope.onCalendar = true;
					$scope.getCalendar(appointments);
				})
				.catch(function(err){
					responseService.openModal(err)
						.then(function(result){
							console.log(result);
						}, function(result){
							console.log(result);
						});
				})
		}
		$scope.takeAppointment = function (index){
			$scope.doc_list = [$scope.doc_list[index]];
			$scope.getAppointmentsList($scope.doc_list[0]._id);
		}
		function open(size,payload){
			var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : './app/sharedComponents/appointmentOperationModal/appointmentOperationView.html',
				controller : 'appointmentOperationController',
				size : size,
				resolve : {
					payload : function (){
						return payload;
					}
				}
			})
			return modalInstance;
		}

	}])