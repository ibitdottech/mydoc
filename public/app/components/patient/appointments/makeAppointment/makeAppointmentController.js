angular.module('myDoctor')
	.controller('makeAppointmentController', ['$scope','makeAppointmentService','$uibModalInstance', 'docObj', function ($scope, makeAppointmentService, $uibModalInstance, docObj){
		$scope.ok = function (){
			$uibModalInstance.close();
		}
	}])