angular.module('myDoctor')
	.service('makeAppointmentService', ['$http', function  ($http){
		var req = {};
		this.search = function (data){
			req = {
				'method' : 'POST',
				'url' :  '/users/login',
				'header' : {
					"Content-Type" : "application/json"
				},
				"skipAuthorization": true,
				'data' : data
			}
			return $http(req);
		}
	}])