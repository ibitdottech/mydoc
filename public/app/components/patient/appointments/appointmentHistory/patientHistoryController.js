angular.module('myDoctor')
	.controller('patientHistoryController', ['$scope','patientHistoryService', function ($scope, patientHistoryService){
		var jq = $.noConflict();
		$scope.noData = false;
		$scope.getHistory = function(){
			console.log("Herer");
			patientHistoryService.history()
					.then(function(res){
						console.log(res);
						if(res.data.length == 0)
							$scope.noData = true;
						else{
							$scope.historyObj = res.data;
							jq("#example1").DataTable();
						}
					})
		}
		$scope.getHistory();
	}])