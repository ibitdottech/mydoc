angular.module('myDoctor')
	.service('patientHistoryService', ['$http','Values', function  ($http,Values){
		var req = {};
		this.history = function (){
			req = {
				'method' : 'GET',
				'url' :Values.baseUrl +'/patient/my-history'
			}
			return $http(req);
		}
	}])