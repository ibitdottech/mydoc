angular.module('myDoctor')
	.controller('registerController',['$scope','registerService', 'responseService', '$state','$location',function ($scope, registerService, responseService, $state,$location) {
		var today = new Date();
		$scope.lists = {
			gender : ['Male','Female', 'Other'],
			status : ['Married', 'Single']
		}
		$scope.userData = {};
		$scope.userData.contacts = [];
		$scope.userData.profile_pic = "http://localhost:3000/assets/imgs/user-default.png";
		$scope.userData.gender = "";
		$scope.userData.marital_status = "";
		$scope.userData.joined_date = today.toUTCString();

		$scope.passwordValidator = function(password) {

			if(!password){return;}

			if (password.length < 8) {
				return "Password must be at least " + 8 + " characters long";
			}

			if (!password.match(/[A-Z]/)) {
				return "Password must have at least one capital letter";
			}

			if (!password.match(/[0-9]/)) {
				return "Password must have at least one number";
			}

			return true;
		};
		
		$scope.register = function(){
			console.log($scope.userData)
			if ($scope.newPatient.file.$valid && $scope.profilePic) {
				
				registerService.upload($scope.profilePic)
				.then(function (res){
					console.log(res.data);
					$scope.userData.profile_pic = res.data.url;
					console.log($scope.userData);
					return registerService.register($scope.userData);
				})
				.then(function (res){
					console.log(res.data);
					$state.go('login');
				})
				.catch(function (err){
					responseService.openModal(err)
						.then(function(res){
							console.log(res);
						})
				});
			}else{
				registerService.register($scope.userData)
				.then(function (res){
					console.log(res.data);
					$state.go('login');
				})
				.catch(function (err){
					responseService.openModal(err)
						.then(function(res){
							console.log(res);
						})
				});
			}
		}
		$scope.selected = function(arg){
			console.log(arg);
		}
	}])