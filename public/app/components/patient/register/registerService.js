angular.module("myDoctor")
	.service('registerService', ['Upload', '$http', 'Values', function  (Upload, $http, Values){
		var req = {};
		this.register = function (data){
			req = {
				'method' : 'POST',
				'url' : Values.baseUrl + '/patient/register',
				'header' : {
					"Content-Type" : "application/json"
				},
				"skipAuthorization": true,
				'data' : data
			}
			return $http(req);
		}
		this.upload = function(file){
			req = {
				'method' : 'POST',
				'url' : Values.baseUrl + '/user/uploads',
				'headers' : {'Content-Type': 'multipart/form-data'},
				'data' : {file : file}
			}
			return Upload.upload(req);
		}
	}])