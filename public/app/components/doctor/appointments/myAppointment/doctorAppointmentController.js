angular.module('myDoctor')
	.controller('doctorAppointmentController', ['$scope', '$rootScope', 'responseService', 'doctorAppointmentService', '$uibModal', function ($scope, $rootScope, responseService, doctorAppointmentService, $uibModal){
		var jq = $.noConflict();
		$scope.appointments = [];
		$scope.appointmentsDetail = [];
		$scope.getCalendar = function(){
			console.log($rootScope.User);
			jq('#calendar').fullCalendar({
				eventClick: $scope.eventClick,
				// dayClick: $scope.dayClick,
				timezone : 'local',
				events : $scope.appointments,
				slotDuration : '00:' + $rootScope.User.availability.slot,
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				buttonText: {
					today: 'today',
					month: 'month',
					week: 'week',
					day: 'day'
				},
				editable: true,
				eventOverlap : false,
				droppable: false,
			})
		}
		$scope.getAppointment = function(){
			doctorAppointmentService.getAppointments($rootScope.User._id)
				.then(function (res){
					console.log(res);
					if(res.data.calendarData.length != 0){
						$scope.appointmentsDetail = res.data.appointmentData;
						var i = 0;
						$scope.appointments = res.data.calendarData.map(function(app){
							app.start = new Date(app.start);
							app.end = new Date (app.end);
							app.index = i++;
							if(app.isFulfilled){
								app.backgroundColor = "#00a65a";
								app.title += " (Fulfilled)";
								app.borderColor = "#00a65a";
							}
							else if(app.isCanceled){
								app.backgroundColor = "#dd4b39";
								app.borderColor = "#dd4b39";
								app.title += " (Canceled)";
							}
							else{
								app.backgroundColor = "#3c8dbc";
								app.borderColor = "#3c8dbc";
							}
							return app;
						})
					}
					$scope.getCalendar();
				})
				.catch(function (err){
				})
		}
		$scope.eventClick = function(calEvent, jsEvent, view){
			$scope.makePrescription(calEvent.index);
		}
		$scope.getAppointment();
		$scope.makePrescription = function (index){
			const modalInstance = $uibModal.open({
				animation : true,
				templateUrl : './app/components/doctor/addPrescription/addPrescriptionView.html',
				controller : 'addPrescriptionController',
				size : 'lg',
				resolve : {
					patientObj : function (){
						return $scope.appointmentsDetail[index];
					}
				}
			})
			modalInstance.result
				.then(function(res){
					if(res == 'cancel')
						cancelAppointment(index);
					$scope.getAppointment();
				})
				.catch(function(err){
					console.log(err);
				})
		}
		function cancelAppointment(index){
			var payload = {
				title : "Cancel Appointment with " + $scope.appointmentsDetail[index].patient.first_name,
				description : "Are you sure you?"
			}
			var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : './app/sharedComponents/promptModal/promptView.html',
				controller : 'promptController',
				size : 'sm',
				resolve : {
					payload : function (){
						return payload;
					}
				}
			})
			modalInstance.result.then(function(res){
				console.log(res);
				if(res){
					doctorAppointmentService.cancelAppointment({appointment_id :$scope.appointmentsDetail[index]._id})
					.then(function(res){
						showResponse(res);
					})
					.catch(function(err){
						showResponse(res);
					})
				}
			})
		}
		function showResponse (payload){
			responseService.openModal(payload)
				.then(function(res){
					console.log(res);
				})
				.catch(function(err){
					console.log(err);
				})
		}
	}])