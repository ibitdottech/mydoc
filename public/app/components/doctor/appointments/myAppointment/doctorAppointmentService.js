angular.module('myDoctor')
	.service('doctorAppointmentService', ['$http','Values', function  ($http,Values){
		var req = {};
		var baseUrl = Values.baseUrl;
		this.getAppointments = function (id){
			req = {
				'method' : 'GET',
				'url' :  baseUrl + '/doctor/my-appoinments/' + id,
			}
			return $http(req);
		}
		this.cancelAppointment = function (data){
			req = {
				'method' : 'POST',
				'url' :  Values.baseUrl + '/doctor/cancel-appointment',
				'data' : data
			}
			return $http(req);
		}
	}])