"use strict";
angular.module('myDoctor')
	.controller('addPrescriptionController', ['$scope', 'addPrescriptionService', 'responseService', '$uibModalInstance', 'patientObj', function ($scope, addPrescriptionService, responseService, $uibModalInstance, patientObj){
		$scope.Data = patientObj;
		$scope.historyData = [];
		$scope.isHistory = false;
		$scope.obj = {
			appointment_id : $scope.Data._id,
			prescription : {}
		}
		$scope.cancelAppointment = function (){
			$uibModalInstance.close('cancel');
		}
		$scope.addPrescription = function(){
			addPrescriptionService.closeAppointment($scope.obj)
				.then(function(res) {
					return responseService.openModal(res);
				})
				.then(function(res){
					$uibModalInstance.close(false);
				})
				.catch(function(err){
					responseService.openModal(err)
						.then(function(res){
							$uibModalInstance.close(false);
						})
						.catch(function(err){
							$uibModalInstance.close(false);
						})
				})
		}
		function patientHistory(){
			console.log($scope.Data)
			addPrescriptionService.getPatientHistory($scope.Data.patient._id)
				.then(function(res) {
					$scope.historyData = res.data;
				})
				.catch(function (err){
					responseService.openModal(err)
					.then(function(res){
						console.log(res);
					})
					.catch(function(err){
						console.log(err);
					})
				})
		}
		$scope.switch = function(){
			// if($scope.isHistory)
			// 	jq("#example1").DataTable();
			// else
			// 	jq("#example1").DataTable().destroy();
			if($scope.historyData.length == 0){
				patientHistory();
			}
			$scope.isHistory = !$scope.isHistory;
		}
	}])