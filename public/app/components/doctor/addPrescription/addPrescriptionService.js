angular.module('myDoctor')
	.service('addPrescriptionService', ['$http', 'Values', function  ($http, Values){
		var req = {};
		this.cancelAppointment = function (data){
			req = {
				'method' : 'POST',
				'url' :  Values.baseUrl + '/doctor/cancel-appointment',
				'data' : data
			}
			return $http(req);
		}
		this.closeAppointment = function(data){
			req = {
				'method' : 'POST',
				'url' :  Values.baseUrl + '/doctor/add-prescription',
				'data' : data
			}
			return $http(req);
		}
		this.getPatientHistory = function(id){
			req = {
				'method' : 'GET',
				'url' :  Values.baseUrl + '/doctor/patient-history/' + id,
			}
			return $http(req);
		}
	}])