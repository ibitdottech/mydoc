angular.module('myDoctor')
	.controller('updateSpecializationController', ['$scope','$uibModalInstance', 'specObj', function ($scope, $uibModalInstance, specObj){
		$scope.updatedSpec = specObj;
		$scope.update = function (){
			$uibModalInstance.close($scope.updatedSpec);
		}
		$scope.cancel = function (){
			$uibModalInstance.dismiss('cancel');
		}
	}])