angular.module('myDoctor')
	.service('specializationService', ['$http', 'Values', function  ($http,Values){
		var req = {};
		this.updateSpecialization = function(data){
			req = {
				'method' : 'POST',
				'url' : Values.baseUrl + '/admin/update-specialization',
				data : data,
			}
			return $http(req);
		}
		this.removeSpec = function(data){
			req = {
				'method' : 'POST',
				'url' : Values.baseUrl + '/admin/remove-specialization',
				data : data,
			}
			return $http(req);
		}
		this.addSpecialization = function (data){
			req = {
				'method' : 'POST',
				'url' : Values.baseUrl + '/admin/add-specialization',
				data : data,
			}
			return $http(req);
		}
	}])