angular.module('myDoctor')
	.controller('specializationController',['$scope', 'specializationService', 'getSpecializationService', 'responseService', '$state', '$uibModal', '$location',function ($scope, specializationService, getSpecializationService, responseService, $state, $uibModal, $location) {
		$scope.spec_list = [];
		$scope.newSpec = {};
		$scope.oneSpec = {};
		$scope.getAllSpec = function(){
			getSpecializationService.getAll()
				.then(function (res){
					console.log(res);
					$scope.spec_list = res.data;
				})
				.catch(function (err){
					console.log(err);
				})
		}
		$scope.addSpec = function(){
			console.log("herer");
			var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : './app/components/admin/manageSpecialization/addSpecialization/addSpecializationView.html',
				controller : 'addSpecializationController',
				size : 'sm',
			})
			modalInstance.result.then(function(res){
				return specializationService.addSpecialization({name : res})
			})
			.then(function (res){
				console.log(res);
				$scope.getAllSpec();
			})
			.catch(function (err){
				console.log(err);
			})
		};
		$scope.getAllSpec();
		$scope.removeSpec = function(index){
			var payload = {
				title : "Remove " + $scope.spec_list[index].name,
				description : "Are you sure?"
			}
			var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : './app/sharedComponents/promptModal/promptView.html',
				controller : 'promptController',
				size : 'sm',
				resolve : {
					payload : function (){
						return payload;
					}
				}
			})
			modalInstance.result.then(function (action){
				if(action){
					specializationService.removeSpec($scope.spec_list[index])
						.then(function (res){
							console.log(res);
							$scope.getAllSpec();
						}, function(err){
							responseService.openModal(err)
								.then(function(result){
									console.log(result);
								}, function(result){
									console.log(result);
								})
						})
						.catch(function (err){
						})
				}
			})
		};
		$scope.updateSpec = function(index){
			var modalInstance = $uibModal.open({
				animation : true,
				templateUrl : './app/components/admin/manageSpecialization/updateSpecialization/updateSpecializationView.html',
				controller : 'updateSpecializationController',
				size : 'sm',
				resolve : {
					specObj : function (){
						return $scope.spec_list[index];
					}
				}
			})
			modalInstance.result.then(function (result){
				console.log(result);
				return specializationService.updateSpecialization(result);
			})
			.then(function (result){
				console.log(result);
				$scope.getAllSpec();
			})
			.catch(function (err){
				responseService.openModal(err)
								.then(function(result){
									console.log(result);
								})
								.catch(function(result){
									console.log(result);
								})
			})
		};

	}])