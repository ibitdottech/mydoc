angular.module('myDoctor')
	.controller('addSpecializationController', ['$scope','$uibModalInstance', function ($scope, $uibModalInstance){
		$scope.addSpec = function (){
			$uibModalInstance.close($scope.name);
		}
		$scope.cancel = function(){
			$uibModalInstance.dismiss('cancel');
		}
	}])