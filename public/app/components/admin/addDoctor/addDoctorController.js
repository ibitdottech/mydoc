angular.module('myDoctor')
	.controller('addDoctorController', ['$scope', 'addDoctorService', 'responseService', 'getSpecializationService', 'specializationService', '$state', '$location', function ($scope, addDoctorService, responseService, getSpecializationService, specializationService, $state, $location) {
		var jq = $.noConflict();
		var today = new Date();
		$scope.selectedSpec = [];
		$scope.settings = {
			smartButtonMaxItems : 3,
			displayProp : 'label',
			enableSearch : true,
			idProp: 'label',
			smartButtonTextConverter : function(itemText, originalItem) {
				return itemText;
			}
		};
		$scope.lists = {
			gender : ['Male','Female', 'Other'],
			status : ['Married', 'Single'],
			spec : []
		}
		getSpecializationService.getAll()
			.then(function (res){
				if(res.data instanceof Array){
					for (var i = 0; i < res.data.length; i++) {
						var obj = {id : res.data[i]._id, label : res.data[i].name}
						$scope.lists.spec.push(obj);
					};
				$scope.selectedSpec.push({id : $scope.lists.spec[0].label});
				}
			})
			.catch(function (err){
				console.log(err);
			})
		$scope.days = [false,false,false,false,false,false,false];
		$scope.newDoctor = {
			profile_pic : "http://localhost:3000/assets/imgs/user-default.png",
			first_name : '',
			last_name : '',
			gender : 'Male',
			marital_status : 'Single',
			specialization : [],
			availability : {
				days : []
			},
			joined_date : today.toUTCString()
		};
		console.log($scope.newDoctor);
		$scope.addDay = function(value){
			var breaked = false;
			
			for (var i = 0; i < $scope.newDoctor.availability.days.length; i++) {
				if($scope.newDoctor.availability.days[i] === value){
					$scope.newDoctor.availability.days.splice(i,1);
					breaked = true;
					break;
				}
			};
			if(!breaked){
				$scope.newDoctor.availability.days.push(value);
			}
		};

		$scope.passwordValidator = function(password) {

			if(!password){return;}

			if (password.length < 8) {
				return "Password must be at least " + 8 + " characters long";
			}

			if (!password.match(/[A-Z]/)) {
				return "Password must have at least one capital letter";
			}

			if (!password.match(/[0-9]/)) {
				return "Password must have at least one number";
			}

			return true;
		};
		$scope.changeType = function(){
			if($scope.newDoctor.type === 'Rented'){
				jq('#datepicker').attr({'required' : true});
			}else if($scope.newDoctor.type === 'Comissioned'){
				jq('#datepicker').removeAttr('required');
			}
		}
		$scope.register = function(){
			console.log(":herer");
			var obj1 = $scope.newDoctor.availability.from.split(" ");
			var obj2 = $scope.newDoctor.availability.to.split(" ");
			obj1[0] = obj1[0].replace(":", "");
			obj2[0] = obj2[0].replace(":", "");
			if(obj1[1] != "AM" && obj2[1] == "AM"){
				console.log("IF 1");
				$scope.rangeError = "From Time Must be in AM and end time must be in PM";
				return
			}
			if((obj1[1] == obj2[1]) && (obj1[0] != "1200") && (obj1[0] > obj2[0])){
				$scope.rangeError = "To time must be greater than From time.";
				console.log("IF 2");
				return;
			}
			$scope.rangeError = null;
			$scope.newDoctor.specialization = $scope.selectedSpec.map(function (ele){
				var value = ele.id;
				return value;
			})
			if ($scope.addNewDoc.file.$valid && $scope.profilePic) {

				addDoctorService.upload($scope.profilePic)
				.then(function (res){
					console.log(res.data);
					$scope.newDoctor.profile_pic = res.data.url;
					return addDoctorService.addDoc($scope.newDoctor);
				})
				.then(function (res){
					responseService.openModal(res)
						.then(function(result) {
							console.log(result);
						})
						.catch(function(result) {
							console.log(result);
						});
				})
				.catch(function (err){
					responseService.openModal(err)
						.then(function(result) {
							console.log(result);
						})
						.catch(function(result) {
							console.log(result);
						})
				});
			}else{
				addDoctorService.addDoc($scope.newDoctor)
				.then(function (res){
					responseService.openModal(res)
						.then(function(result) {
							console.log(result);
						})
						.catch(function(result) {
							console.log(result);
						})
				})
				.catch(function (err){
					responseService.openModal(err)
						.then(function(result) {
							console.log(result);
						})
						.catch(function(result) {
							console.log(result);
						})
				});
			}

		}
	}])