angular.module("myDoctor")
	.service('addDoctorService', ['Upload', '$http', 'Values', function  (Upload, $http, Values){
		var req = {};
		this.addDoc = function (data){
			req = {
				'method' : 'POST',
				'url' : Values.baseUrl + '/admin/add-doctor',
				'header' : {
					"Content-Type" : "application/json"
				},
				'data' : data
			}
			return $http(req);
		}
		this.upload = function(file){
			req = {
				'method' : 'POST',
				'url' : Values.baseUrl + '/user/uploads',
				'headers' : {'Content-Type': 'multipart/form-data'},
				"skipAuthorization": true,
				'data' : {file : file}
			}
			return Upload.upload(req);
		}
	}])