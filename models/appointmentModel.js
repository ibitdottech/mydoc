const mongoose = require("mongoose"),
	Schema = mongoose.Schema,

	appoinmentSchema = new Schema ({
		patient : {
			"_id" : Schema.Types.ObjectId,
			"first_name" : {type : String, required : true, trim : true},
			"last_name" : {type : String, required : true, trim : true},
			"username" : {type : String, required : true, trim : true},
			"email" : {type : String, required : true, trim : true},
			"role" : [],
			"profile_pic" : {type : String},
			"dob" : Date,
			"joined_date" : {type : Date, default : Date.now()},
			"gender" : {type : String, default : null, trim : true},
			"marital_status" : {type : String, default : null, trim : true},
			"contacts" : [],
			"address" : {
				"address_detail" : String,
				"city" : String,
				'state' : String,
				"country" : String
			}
		},
		doctor : {
			"_id" : Schema.Types.ObjectId,
			"first_name" : {type : String, required : true, trim : true},
			"last_name" : {type : String, required : true, trim : true},
			"username" : {type : String, required : true, trim : true},
			"email" : {type : String, required : true, trim : true},
			"role" : [],
			"profile_pic" : {type : String},
			"dob" : Date,
			"joined_date" : {type : Date, default : Date.now()},
			"gender" : {type : String, default : null, trim : true},
			"marital_status" : {type : String, default : null, trim : true},
			"contacts" : [],
			"address" : {
				"address_detail" : String,
				"city" : String,
				'state' : String,
				"country" : String
			},
			"specialization" : {type : Array, required : true, trim : true},
			"type" : {type : String, required : true, trim : true},
			"about" : String,
			"availability" : {
				"from" : {type : String, required : true, trim : true},
				"to" : {type : String, required : true, trim : true},
				"slot" : {type : Number, required : true, trim : true},
				"days" : {type : Array, required : true, trim : true}
			},
			"due_date" : {type : Date, required : true, trim : true}
		},
		time : {
			from : {type : Date, required : true},
			to : {type : Date, required : true}
		},
		prescription : {
			detail : {type : String, default : "No Prescription", trim : true},
			diagnosis : {type : String, trim : true},
			diagnostic : {type : String, trim : true},
		},
		isBooked : Boolean,
		isCanceled : Boolean,
		isFulfilled : Boolean
	})
	appointmentModel = mongoose.model('appoinments', appoinmentSchema);

module.exports = appointmentModel;