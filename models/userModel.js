'use strict';
const mongoose = require('mongoose'),
	Schema = mongoose.Schema,

	userSchema = new Schema({
		"first_name" : {type : String, required : true, trim : true},
		"last_name" : {type : String, required : true, trim : true},
		"username" : {type : String, unique : true, required : true, trim : true},
		"email" : {type : String, unique : true, required : true, trim : true},
		"password" : {type : String, required : true, trim : true},
		"role" : [],
		"profile_pic" : {type : String},
		"dob" : Date,
		"joined_date" : {type : Date, default : Date.now()},
		"gender" : {type : String, default : null, trim : true},
		"marital_status" : {type : String, default : null, trim : true},
		"contacts" : [],
		"address" : {
			"address_detail" : String,
			"city" : String,
			'state' : String,
			"country" : String
		}
	}),
	userModel = mongoose.model('users', userSchema);

module.exports = userModel;