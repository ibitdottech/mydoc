'use strict';
const mongoose = require("mongoose"),
	Schema = mongoose.Schema,
	errFunc = require('../lib/errorFunctions'),
	_var = require('../lib/variables'),
	q = require('q'),

	doctorSchema = new Schema({
		"first_name" : {type : String, required : true, trim : true},
		"last_name" : {type : String, required : true, trim : true},
		"username" : {type : String, unique : true, required : true, trim : true},
		"email" : {type : String, unique : true, required : true, trim : true},
		"password" : {type : String, required : true, trim : true},
		"role" : [],
		"profile_pic" : {type : String},
		"dob" : Date,
		"joined_date" : {type : Date, default : Date.now()},
		"gender" : {type : String, default : null, trim : true},
		"marital_status" : {type : String, default : null, trim : true},
		"contacts" : [],
		"address" : {
			"address_detail" : String,
			"city" : String,
			'state' : String,
			"country" : String
		},
		"specialization" : {type : Array, required : true, trim : true},
		"type" : {type : String, required : true, trim : true},
		"about" : String,
		"availability" : {
			"from" : {type : String, required : true, trim : true},
			"to" : {type : String, required : true, trim : true},
			"slot" : {type : Number, required : true, trim : true},
			"days" : {type : Array, required : true, trim : true}
		},
		"due_date" : {type : Date, required : true, trim : true}
	});

doctorSchema.statics.getDocById = function getDocById(request){
	const id = request.params.UID;
	return this.findOne({'_id' : id},{'password' : 0});
}
doctorSchema.statics.updateDoctor = function updateDoctor(request){
	
	const defered = q.defer(),
		id = request.body._id;
	this.findById(id)
		.then(doctor => {
			if(doctor == null)
				return defered.reject(_var.ND);
			doctor.first_name = request.body.first_name;
			doctor.last_name = request.body.last_name;
			doctor.contacts = request.body.contacts;
			doctor.email = request.body.email;
			doctor.address = request.body.address;
			doctor.specialization = request.body.specialization;
			doctor.type = request.body.type;
			doctor.about = request.body.about;
			doctor.availability = request.body.availability;
			if(request.role == 'admin')
				doctor.due_date = request.body.due_date;
			return doctor.save();
		})
		.then(updatedDoctor => {
			defered.resolve(updatedDoctor);
		})
		.catch(err => {
			let res = _var.SE;
			let status = 500;
			if(err.code == 11000){
				res = errFunc.duplicate(err.errmsg);
				status = 400;
			}
			if(err.name == "ValidationError"){
				res = errFunc.required(err.errors);
				status = 400;
			}
			defered.reject(res,status);
		})
	return defered.promise;
}
const doctorModel = mongoose.model('doctors', doctorSchema);
module.exports = doctorModel;