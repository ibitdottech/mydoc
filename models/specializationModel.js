'use strict';
const mongoose = require("mongoose"),
	Schema = mongoose.Schema,
	
	specializationSchema = new Schema({
		"name" : {type : String, unique : true, required : true, trim : true}
	});

const specializationModel = mongoose.model('specialization', specializationSchema);
module.exports = specializationModel;